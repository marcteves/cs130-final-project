### IMPORTANT ###
# The way of processing and classifying the data set must be consistent.
# i.e. if one is using the MANUAl version for Data Processing, make sure that the Visualizing the Decision Region, and Classifying the Testing Set is also MANUAL
# The same goes for the AUTOMATED version.
# Remember to change the codes to reflect the version.
#################


### Data Preprocessing Manual ###
import pandas as pd
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)

import numpy as np
y=np.append(df.iloc[0:40, 4].values, df.iloc[50:90, 4].values)
y = np.where(y == 'Iris-setosa', -1, 1)
# X = np.append((df.iloc[0:40, [0, 1]].values), (df.iloc[50:90, [0, 1]].values), axis = 0) # x = sepal length, y = sepal width
X = np.append((df.iloc[0:40, [0, 2]].values), (df.iloc[50:90, [0, 2]].values), axis = 0) # x = sepal length, y = petal length
# X = np.append((df.iloc[0:40, [1, 2]].values), (df.iloc[50:90, [1, 2]].values), axis = 0) # x = sepal width, y = petal length
# X = np.append((df.iloc[0:40, [0, 3]].values), (df.iloc[50:90, [0, 3]].values), axis = 0) # x = sepal length, y = petal width
# X = np.append((df.iloc[0:40, [1, 3]].values), (df.iloc[50:90, [1, 3]].values), axis = 0) # x = sepal width, y = petal width
# X = np.append((df.iloc[0:40, [2, 3]].values), (df.iloc[50:90, [2, 3]].values), axis = 0) # x = petal length, y = petal width
##########

### Data Preprocessing automated interactive changing of features ###
# import pandas as pd
# df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)
#
# import numpy as np
# params = []
#
# params.append(" sepal length in [cm]")
# params.append(" sepal width in [cm]")
# params.append(" petal length in [cm]")
# params.append(" petal width in [cm]")
#
# for idx, param in enumerate(params):
#     print(str(idx) + param)
#
# # be careful with the next two lines of code, especially when one is copy pasting the entire source code to Python's in-line terminal.
# # Make sure to copy and paste just one line of code (for the inputting of the parameters), then press enter, to be able to input the first parameter.
# # After inputting, copy paste the next line of code, press enter, then input the second parameter.
#
# par1 = int(input("Enter first parameter: "))
# par2 = int(input("Enter second parameter: "))
#
# y = np.append(df.iloc[0:40, 4].values, df.iloc[50:90, 4].values)
# y = np.where(y == 'Iris-setosa', -1, 1)
# X = np.append((df.iloc[0:40, [par1, par2]].values), (df.iloc[50:90, [par1, par2]].values), axis = 0)
##########

# Training the Perceptron Model
from perceptron import Perceptron
pn = Perceptron(0.1,10)
pn.fit(X,y)
######

### Number of misclassification errors vs. iteration number ###
# import matplotlib.pyplot as plt
# plt.plot(range(1, len(pn.errors) + 1), pn.errors, marker='o')
# plt.xlabel('Epochs')
# plt.ylabel('Number of misclassifications')
# plt.show()
##########

### Visualizing the Decision Region ###
from matplotlib.colors import ListedColormap
def plot_decision_regions(X, y, classifier, resolution=0.02):
   # setup marker generator and color map
   markers = ('s', 'x', 'o', '^', 'v')
   colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
   cmap = ListedColormap(colors[:len(np.unique(y))])
   # plot the decision surface
   x1_min, x1_max = X[:,  0].min() - 1, X[:, 0].max() + 1
   x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
   xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
   Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
   Z = Z.reshape(xx1.shape)
   plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
   plt.xlim(xx1.min(), xx1.max())
   plt.ylim(xx2.min(), xx2.max())
   # plot class samples
   for idx, cl in enumerate(np.unique(y)):
      plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1], alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)

plot_decision_regions(X, y, classifier=pn)
# Manual version
plt.xlabel('petal length [cm]')

# use this line below, and remove the line above, if the changing of features is automated
# plt.xlabel(params[par1])

# Manual version
plt.ylabel('petal width [cm]')

# use this line below, and remove the line above, if the changing of features is automated
# plt.ylabel(params[par2])

plt.legend(loc='upper left')
plt.show()
##########

### Classifying the Testing Set Manual ###
from numpy import array

testY = np.append(df.iloc[40:50, 4].values, df.iloc[90:100, 4].values)
testY = np.where(testY == 'Iris-setosa', -1, 1)
# testX = np.append((df.iloc[40:50, [0, 1]].values), (df.iloc[90:100, [0, 1]].values), axis = 0) # x = sepal length, y = sepal width
testX = np.append((df.iloc[40:50, [0, 2]].values), (df.iloc[90:100, [0, 2]].values), axis = 0) # x = sepal length, y = petal length
# testX = np.append((df.iloc[40:50, [1, 2]].values), (df.iloc[90:100, [1, 2]].values), axis = 0) # x = sepal width, y = petal length
# testX = np.append((df.iloc[40:50, [0, 3]].values), (df.iloc[90:100, [0, 3]].values), axis = 0) # x = sepal length, y = petal width
# testX = np.append((df.iloc[40:50, [1, 3]].values), (df.iloc[90:100, [1, 3]].values), axis = 0) # x = sepal width, y = petal width
# testX = np.append((df.iloc[40:50, [2, 3]].values), (df.iloc[90:100, [2, 3]].values), axis = 0) # x = petal length, y = petal width

result=[]
for x in testX:
    if pn.predict(x) == array(1):
        result.append(1)
    else:
        result.append(-1)
##########

### Classifying the Testing Set automated changing of features ###
# from numpy import array
#
# testY = np.append(df.iloc[40:50, 4].values, df.iloc[90:100, 4].values)
# testY = np.where(testY == 'Iris-setosa', -1, 1)
# testX = np.append((df.iloc[40:50, [par1, par2]].values), (df.iloc[90:100, [par1, par2]].values), axis = 0)
#
# result=[]
# for x in testX:
#     if pn.predict(x) == array(1):
#         result.append(1)
#     else:
#         result.append(-1)
##########
