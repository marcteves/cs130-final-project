# for results

from perceptron import Perceptron
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap
from numpy import array

df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)

params = []

params.append(" sepal length in cm")
params.append(" sepal width in cm")
params.append(" petal length in cm")
params.append(" petal width in cm")

for idx, param in enumerate(params):
    print(str(idx) + param)

par1 = int(input("Enter first parameter: "))
par2 = int(input("Enter second parameter: "))

testY = np.append(df.iloc[40:50, 4].values, df.iloc[90:100, 4].values)
testY = np.where(testY == 'Iris-setosa', -1, 1)

y = np.append(df.iloc[0:40, 4].values, df.iloc[50:90, 4].values)
y = np.where(y == 'Iris-setosa', -1, 1)

X = np.append((df.iloc[0:40, [par1, par2]].values), (df.iloc[50:90, [par1, par2]].values), axis = 0)
testX = np.append((df.iloc[40:50, [par1, par2]].values), (df.iloc[90:100, [par1, par2]].values), axis = 0)

pn = Perceptron(0.1, 10)
pn.fit(X, y)

result=[]
for x in testX:
    if pn.predict(x) == array(1):
        result.append(1)
    else:
        result.append(-1)


accuracy = 10

# 1st column: Desired Output. 2nd column: Actual Output
# 1: Iris Versicolor. -1: Iris Setosa
for (desired, actual) in zip(testY.tolist(), result):
    if(desired != actual):
        accuracy -= 1
    print('{}\t{}'.format(desired, actual))

print("Accuracy: " + str(accuracy*10) + "%")


def plot_decision_regions_results(X, y, testX, testY, classifier, resolution=0.02):
   # setup marker generator and color map
   markers = ('s', 'x', 'o', '^', 'v')
   colors = ('red', 'blue', 'lightgreen', 'yellow', 'cyan')
   cmap = ListedColormap(colors[:len(np.unique(y))])
   # plot the decision surface
   totalX = np.append(X, testX, axis=0)
   x1_min, x1_max = totalX[:,  0].min() - 1, totalX[:, 0].max() + 1
   x2_min, x2_max = totalX[:, 1].min() - 1, totalX[:, 1].max() + 1
   xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
   np.arange(x2_min, x2_max, resolution))
   Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
   Z = Z.reshape(xx1.shape)
   plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
   plt.xlim(xx1.min(), xx1.max())
   plt.ylim(xx2.min(), xx2.max())
   cmap = ListedColormap(colors[:len(np.unique(y)) + 2])
   # plot class samples
   for idx, cl in enumerate(np.unique(y)):
      plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],
      alpha=0.8, c=cmap(idx),
      marker=markers[idx], label=cl)
   # plot class samples for test
   for idx, cl in enumerate(np.unique(testY)):
      plt.scatter(x=testX[testY == cl, 0], y=testX[testY == cl, 1],
      alpha=0.8, c=cmap(idx + 2),
      marker=markers[idx], label=cl)


plot_decision_regions_results(X, y, testX, testY, classifier=pn)
plt.xlabel(params[par1])
plt.ylabel(params[par2])
plt.legend(loc='upper left')
plt.show()
